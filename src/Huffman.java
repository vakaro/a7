
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

    private final ArrayList<Leaf> characters;
    private final Leaf root;
    private int bitLength;
    private final Map<Byte, String> huffmanCodes = new HashMap<>(); // Used to store the Huffman codes corresponding to characters


    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param original source data
     */
    Huffman(byte[] original) {
        if (original == null) {
            throw new RuntimeException("Input can not be Null");
        }
        characters = frequency(original);
        root = createTree();
        if (root.isLeaf()) {
            huffmanCodes.put(root.byteCode, "0");
        } else {
            getCodes(root, "");
        }
    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        return bitLength;
    }

    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {

        if (origData == null) {
            throw new RuntimeException("Input can not be Null");
        }

        StringBuilder s = new StringBuilder();

        for (byte bt : origData) {
            s.append(huffmanCodes.get(bt));
        }
        bitLength = s.length();

        byte[] resultBytes = new byte[s.length() % 8 == 0 ? s.length() / 8 : s.length() / 8 + 1];
        int counter = 0;

        for (int i = 0; i < s.length(); i += 8) {
            if (i + 8 < s.length()) {
                resultBytes[counter++] = (byte) Integer.parseInt(s.substring(i, i + 8), 2);
            } else {
                resultBytes[counter++] = (byte) Integer.parseInt(s.substring(i) + new String(new char[8 - (s.length() - i)]).replace("\0", "0"), 2);
            }
        }

        return resultBytes;
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {

        int counter = 0;
        byte[] res = new byte[root.frequency];
        StringBuilder sb = new StringBuilder();
        for (byte resultByte : encodedData) {
            sb.append(String.format("%8s", Integer.toBinaryString(resultByte & 0xFF)).replace(' ', '0'));
        }
        String codedString = sb.substring(0, bitLength());

        int i = 0;
        Leaf current = root;

        while (i < codedString.length()) {
            if (current.isLeaf()) {
                i++;
            } else {
                while (!current.isLeaf()) {
                    char bit = codedString.charAt(i);
                    if (bit == '1') {
                        current = current.right;
                    } else if (bit == '0') {
                        current = current.left;
                    }
                    i++;
                }
            }

            res[counter++] = current.byteCode;
            current = root;
        }
        return res;
    }

    /**
     * @param data array of string bytes
     * @return ArrayList of unique bytes and their occurrence in bytes array
     */
    public ArrayList<Leaf> frequency(byte[] data) {
        ArrayList<Leaf> characters = new ArrayList<>();
        for (byte bt : data) {
            Leaf leaf = new Leaf(bt, 1);
            int index = characters.indexOf(leaf);
            if (index == -1)
                characters.add(leaf);
            else {
                leaf = characters.get(index);
                leaf.frequency = leaf.frequency + 1;
            }
        }
        return characters;
    }

    /**
     * Leaf - a node with two sub-nodes and occurrence counter
     */
    private static class Leaf implements Comparable<Leaf> {

        Byte byteCode;
        int frequency;
        Leaf left;
        Leaf right;

        public Leaf(Byte byteCode, int frequency) {
            this.byteCode = byteCode;
            this.frequency = frequency;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Leaf leaf = (Leaf) o;
            return byteCode.equals(leaf.byteCode);
        }

        @Override
        public int hashCode() {
            return Objects.hash(byteCode);
        }

        @Override
        public String toString() {
            return "Byte: " + byteCode + " ,frequency :" + frequency;
        }

        public boolean isLeaf() {
            return left == null && right == null;
        }

        @Override
        public int compareTo(Leaf o) {
            return Integer.compare(o.frequency, frequency);
        }
    }

    /**
     * @param leaf root node from which to start search
     * @param code string base to use for string buildup
     */
    public void getCodes(Leaf leaf, String code) {

        StringBuilder s = new StringBuilder();
        s.append(code);
        if (leaf != null) {
            if (leaf.byteCode == null) {
                getCodes(leaf.left, code + "0");
                getCodes(leaf.right, code + "1");
            } else {
                huffmanCodes.put(leaf.byteCode, s.toString());
            }
        }
    }

    /**
     * @return root leaf with attached sub-node leaves.
     */
    public Leaf createTree() {
        while (characters.size() > 1) {
            // First sort
            characters.sort(Collections.reverseOrder());
            // Take out the first two smallest ones in the set
            Leaf left = characters.get(0);
            Leaf right = characters.get(1);
            // Create a node
            Leaf parent = new Leaf(null, left.frequency + right.frequency);
            parent.left = left;
            parent.right = right;
            // Remove two nodes from the collection
            characters.remove(left);
            characters.remove(right);
            // Add the parent node to the collection
            characters.add(parent);
        }
        return characters.get(0);
    }

    /**
     * Main method.
     */
    public static void main(String[] params) {

        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
        byte[] orig = tekst.getBytes();

        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);

        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
    }
}

// https://stackoverflow.com/questions/12310017/how-to-convert-a-byte-to-its-binary-string-representation
// https://enos.itcollege.ee/~jpoial/algoritmid/puustruktuurid.html#AVL
// https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
// https://www.youtube.com/watch?v=zSsTG3Flo-I&t=819s
// https://www.programmersought.com/article/97523520461/
// https://stackoverflow.com/questions/48888329/huffman-encoding-how-to-do-the-compression-dealing-with-bytes